require 'spec_helper'

describe Banzai::Filter::BlobReferenceFilter do
  include FilterSpecHelper
  include FakeBlobHelpers

  let(:project) { create(:project, :public, :repository) }
  let(:repository) { project.repository }
  let(:user) { create(:user) }
  let(:blob) { repository.blob_at('master', 'README.md') }
  let(:fake_path) { "/fake/path/and/file.md" }
  let(:reference) { urls.project_blob_url(project, File.join(project.default_branch, blob.path)) }

  it 'links from blob uri' do
    doc = reference_filter(reference)
    expect(doc.to_html).to match(/<a.+>README.md \(master\)<\/a>/)
  end

  it 'links with blob in sub directory' do
    repository.create_file(user, 'folder/file.md', '...',
                           message: 'Create changelog',
                           branch_name: 'master')

    blob = repository.blob_at('master', 'folder/file.md')

    reference = urls.project_blob_url(project, File.join(project.default_branch, blob.path))
    doc = reference_filter(reference)
    expect(doc.to_html).to match(/<a.+>folder\/file.md \(master\)<\/a>/)
  end

  it 'links with line numbers' do
    reference = "#{self.reference}#L100"
    doc = reference_filter(reference)
    expect(doc.to_html).to match(/<a.+>README.md \(master\) #L100<\/a>/)
  end

  it 'link from non-existent path' do
    reference = urls.project_blob_url(project, File.join(project.default_branch, fake_path))
    doc = reference_filter(reference)
    expect(doc.to_html).to match(/<a.+>fake\/path\/and\/file.md \(master\)<\/a>/)
  end
end
