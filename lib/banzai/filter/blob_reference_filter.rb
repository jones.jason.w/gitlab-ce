module Banzai
  module Filter
    # HTML filter that replaces blob references with links.
    #
    # This filter supports cross-project references.
    class BlobReferenceFilter < AbstractReferenceFilter
      self.reference_type = :blob

      def self.object_class
        Blob
      end

      def self.references_in(text, pattern = Blob.reference_pattern)
        text.gsub(pattern) do |match|
          yield match, $~[:path], $~[:project], $~[:namespace], $~
        end
      end

      def find_object_link_filter_object(link_reference, parent, file_path, matches)
        reference = matches.names.include?("reference") ? matches[:reference] : nil
        ReferenceBlob.new(
          file_path: file_path,
          reference: reference,
          anchor: matches[:anchor]
        )
      end

      def object_link_title(object)
        object.title
      end

      def object_link_text(object, matches)
        object.reference_text
      end
    end

    # The ActiveRecord blob object is implemented in a way that makes including Referable finicky. A  better
    # solution is to have a Reference blob object here in order to better address this issue.
    class ReferenceBlob
      include Referable

      def initialize(file_path:, reference: nil, anchor: nil)
        @file_path, @reference, @anchor = file_path, reference, anchor
      end

      def id
        @file_path
      end

      def title
        reference_text
      end

      def reference_text
        return @reference_text unless @reference_text.nil?

        @reference_text = @file_path
        @reference_text << " (#{@reference})" unless @reference.nil?
        @reference_text << " #{@anchor}" unless @anchor.nil?
        @reference_text
      end

      def to_reference(_from = nil, full: nil)
        reference_text
      end

      def reference_link_text(from = nil)
        to_reference(from)
      end

      # Due to the unique nature of the blob url, we need to create a custom pattern, different that that of what is
      # generated from it's parent. (http://dowmain.name/blob/reference_name/path/to/file.md#L1)
      def self.link_reference_pattern
        @link_reference_pattern ||= %r{
        (?<url>
         #{Regexp.escape(Gitlab.config.gitlab.url)}
         \/#{Project.reference_pattern}
         ?\/#{Regexp.escape("blob")}
         \/(?<reference>(?-mix:(?:[a-zA-Z0-9_\.][a-zA-Z0-9_\-\.]*)))
         \/(?<path>
            (?-mix:(?:[a-zA-Z0-9_\.][\/a-zA-Z0-9_\-\.]*))
           )?
           (?<query>
            \?[a-z0-9_=-]+
              (&[a-z0-9_=-]+)*
           )?
           (?<anchor>\#[a-zA-Z0-9_-]+)?
        )
        }x
      end

      # Pattern used to extract blob references from text
      #
      # This pattern supports cross-project references.
      def self.reference_pattern
        @reference_pattern ||= %r{
        (?:#{Project.reference_pattern}#{reference_prefix})
        (?<reference>(?-mix:(?:[a-zA-Z0-9_\.][a-zA-Z0-9_\-\.]*)))
        (?<path>(?-mix:(?:[a-zA-Z0-9_\.][a-zA-Z0-9_\-\.]*)))
        }x
      end
    end
  end
end
