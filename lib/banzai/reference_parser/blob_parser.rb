module Banzai
  module ReferenceParser
    class BlobParser < BaseParser
      self.reference_type = :blob

      def references_relation
        Blob
      end

      private

      def can_read_reference?(user, ref_project, node)
        can?(user, :download_code, ref_project)
      end
    end
  end
end
